const mongoose=require('mongoose');
const { stringify } = require('uuid');

const DatabaseSchema=mongoose.Schema({
    Name : {
        type : String,
        required: true
    },
    Email:{
        type: String,
    },
    Password:{
        type:String,
    },
    SocialId:{
        type:String
    },
    isGoogleUser:{
        type:Boolean
    },
    isAppleUser:{
        type:Boolean
    },
    isFacebookUser:{
            type:Boolean
    },
    CreatedTime:{
        type : Date,
        default : Date.now
    }
});

module.exports = mongoose.model('Users', DatabaseSchema);