const express = require("express");
const app = express();
const uid = require("uuid");
const mongoose = require("mongoose");
const schema = require("./Schema/Users");
var cors = require("cors");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var morgan = require("morgan");
// require("dotenv/config")

const saltRound = 10;

app.use(morgan("dev"));

app.use(express.json());

app.use(cors());

app.listen(8020, async () => {
  console.log("running on port 8020");
});

mongoose.connect(
  "mongodb+srv://intelense:Intelense123@cluster0.lz7bd.mongodb.net/petapp?retryWrites=true&w=majority",
  async (err) => {
    if (err) {
      return console.log("DB is Not Connected", err);
    }
    console.log("DB is Connected");
  }
);

const Persons = [
  {
    id: uid.v4(),
    name: "Arun",
    age: 20,
  },
  {
    id: uid.v4(),
    name: "John",
    age: 22,
  },
  {
    id: uid.v4(),
    name: "Martin",
    age: 25,
  },
  {
    id: uid.v4(),
    name: "therasa",
    age: 30,
  },
];

app.post("/addperson", async (req, res) => {
  console.log(bcrypt.hash(req.body.password, saltRound));
  await schema
    .findOne({ Email: req.body.email })
    .then(async (dbUser) => {
      if (dbUser) {
        return res.status(201).json({ message: "email already exists" });
      } else if (req.body.email && req.body.password) {
        const password = await bcrypt.hashSync(req.body.password, saltRound);
        const saveperson = new schema({
          Name: req.body.name,
          Email: req.body.email,
          Password: password,
        });
        const saved = saveperson.save();
        console.log("saved");
        res.status(200).json({ message: "user created" });
      } else if (!req.body.password) {
        return res.status(400).json({ message: "password not provided" });
      } else if (!req.body.email) {
        return res.status(400).json({ message: "email not provided" });
      }
    })
    .catch((err) => {
      console.log("error", err);
    });
});

app.post("/googlelogin", async (req, res) => {
  await schema
    .findOne({ SocialId: req.body.id })
    .then(async (dbUser) => {
      if (dbUser) {
        return res.status(200).json({ message: "User Registered Already" });
      } else if (req.body.email && req.body.name) {
        const saveperson = new schema({
          Name: req.body.name,
          Email: req.body.email,
          SocialId: req.body.id,
          isGoogleUser: true,
        });
        const saved = saveperson.save();
        console.log(" google user saved");
        res.status(200).json({ message: "user created" });
      }
    })
    .catch((err) => {
      console.log("error", err);
    });
});

app.post("/facebooklogin", async (req, res) => {
  await schema
    .findOne({ SocialId: req.body.id })
    .then(async (dbUser) => {
      if (dbUser) {
        return res.status(200).json({ message: "User Registered Already" });
      } else if (req.body.id && req.body.name) {
        const saveperson = new schema({
          Name: req.body.name,
          SocialId: req.body.id,
          isFacebookUser: true,
        });
        const saved = saveperson.save();
        console.log(" facebook user saved");
        res.status(200).json({ message: "user created" });
      }
    })
    .catch((err) => {
      console.log("error", err);
    });
});

app.get("/getall", async (req, res) => {
  const getall = await schema.find();
  res.status(200).json(getall);
  // res.json(getall);
});

app.post("/login", async (req, res) => {
  await schema
    .findOne({ Email: req.body.email })
    .then((dbUser) => {
      console.log("dbuser");
      console.log(dbUser);
      if (!dbUser) {
        return res.status(202).json({ message: "user not found" });
      } else {
        // password hash
        console.log("pass");
        console.log(req.body.password);
        console.log(dbUser.Password);
        console.log(dbUser.Name);
        console.log(dbUser.Email);
        console.log(dbUser.id);
        console.log(dbUser);

        bcrypt.compare(
          req.body.password,
          dbUser.Password,
          (err, compareRes) => {
            console.log("cmppass" + compareRes);
            if (err) {
              // error while comparing
              res
                .status(502)
                .json({ message: "error while checking user password" });
            } else if (compareRes) {
              const id = dbUser.id;

              // password match
              // const token = jwt.sign({ email: req.body.email }, 'secret', { expiresIn: '1h' });
              const userToken = jwt.sign(
                { email: dbUser.Email, id: id, name: dbUser.Name },
                "HelloWorld"
              );
              const username = dbUser.Name;
              console.log(typeof username);
              console.log("tokenn  " + userToken);
              // res.status(200).send(userToken, username);
              res.status(200).json({
                message: "user logged in",
                token: userToken,
                username: username,
                userid:id
              });
              // res.status(200).json({message: "user logged in"});
            } else {
              // password doesnt match
              res.status(201).json({ message: "invalid credentials" });
            }
          }
        );
      }
    })
    .catch((err) => {
      console.log("error", err);
    });
});

app.get("/home", async (req, res) => {
  console.log(JSON.parse(req.headers.auth));
  console.log("backend");
  jwt.verify(JSON.parse(req.headers.auth), "HelloWorld", async (err, data) => {
    console.log({ data });
    if (err) {
      return res.status(403).json("Invalid Token");
    } else {
      console.log(data);
      // res.status(200).json("welcome");
      return res.send(data).status(200);
    }
  });
});

// app.get('/get/:id',async(req, res)=>{
//

app.get("/", (req, res) => {
  //   console.log(req.body);

  res.status(200).send("hi");
});

app.get("/about", (req, res) => {
  console.log("hello");
  const person = Persons.filter((per) => {
    console.log(per);
  });
  res.json(Persons);
});
app.post("/add", async (req, res) => {
  console.log(req.body);
  // console.log(req.query);

  // Persons.push(req.body)
  // res.status(300).json(Persons)

  console.log(req.body.name);
});
